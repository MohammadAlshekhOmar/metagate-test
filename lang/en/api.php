<?php

return [

    'loginSuccessfully'                             => 'Login Successfully',
    'logoutSuccessfully'                            => 'Logout Successfully',
    'userIsBlocked'                                 => 'User Is Blocked',
    'passwordDontMatch'                             => 'InCorrect Password',
    'unknownError'                                  => 'Unknown Error',
    'doneSuccessfully'                              => 'Done Successfully',
    'addSuccessfully'                               => 'Add Successfully',
    'editSuccessfully'                              => 'Edit Successfully',
    'deleteSuccessfully'                            => 'Delete Successfully',
    'userExists'                                    => 'User Information Exists',
    'categoryExists'                                => 'Category Exists',
    'movieExists'                                   => 'Movie Exists',

];
