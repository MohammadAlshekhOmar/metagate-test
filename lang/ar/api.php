<?php

return [

    'loginSuccessfully'                             => 'تم تسجيل الدخول بنجاح',
    'logoutSuccessfully'                            => 'تم تسجيل الخروج بنجاح',
    'userIsBlocked'                                 => 'المستخدم غير فعال',
    'passwordDontMatch'                             => 'كلمة المرور غير صحيحة',
    'unknownError'                                  => 'حدث خطأ غير معروف',
    'doneSuccessfully'                              => 'تمت العملية بنجاح',
    'addSuccessfully'                               => 'تمت الإضافة بنجاح',
    'editSuccessfully'                              => 'تم التعديل بنجاح',
    'deleteSuccessfully'                            => 'تم الحذف بنجاح',
    'userExists'                                    => 'معلومات المستخدم',
    'categoryExists'                                => 'التصنيفات الموجودة',
    'movieExists'                                   => 'الأفلام الموجودة',

];
