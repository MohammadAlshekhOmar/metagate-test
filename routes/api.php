<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\MovieController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::controller(AuthController::class)->group(function () {
        Route::post('login', 'login');
    });
});

Route::group(['prefix' => 'users'], function () {
    Route::controller(UserController::class)->group(function () {
        Route::post('register', 'register');
    });
});

Route::middleware('auth:api')->group(function () {

    Route::group(['prefix' => 'auth'], function () {
        Route::controller(AuthController::class)->group(function () {
            Route::get('logout', 'logout');
        });
    });

    Route::group(['prefix' => 'users'], function () {
        Route::controller(UserController::class)->group(function () {
            Route::get('get', 'get');
        });
    });

    Route::group(['prefix' => 'categories'], function () {
        Route::controller(CategoryController::class)->group(function () {
            Route::get('all', 'all');
            Route::post('add', 'add');
            Route::put('edit', 'edit');
            Route::delete('delete', 'delete');
        });
    });

    Route::group(['prefix' => 'movies'], function () {
        Route::controller(MovieController::class)->group(function () {
            Route::get('all', 'all');
            Route::post('add', 'add');
            Route::post('edit', 'edit');
            Route::delete('delete', 'delete');
        });
    });
});
