@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <input type="search" id="searchMovie" class="form-control" placeholder="Find Movie here" name="search">
                <br>
                <div class="card">
                    <div class="card-header">
                        <label>Movies</label>
                    </div>
                    <div class="card-body">
                        <select class="form-control" id="categories">
                            <option value="">Choose Category</option>
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                        <div class="row" id="movies">
                            @foreach ($movies as $movie)
                                <div class="col-md-4">
                                    <div class="card m-2">
                                        <div class="card-header">
                                            <label>{{ $movie->name }}</label>
                                        </div>
                                        <div class="card-body">
                                            <a href="{{ route('movie.detail', ['id' => $movie->id]) }}">
                                                <img src="{{ $movie->image }}" alt="movie image"
                                                    style="width:170px;height:150px;">
                                            </a>
                                        </div>
                                        <div class="card-footer">
                                            <p>{{ Str::limit($movie->description, 20) }}</p>
                                        </div>
                                        @foreach ($movie->categories as $category)
                                            <span class="badge rounded-pill badge-light-info"
                                                style="font-size: 14px;color: darkgoldenrod;">{{ $category->name }}</span>
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        {{ $movies->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('#categories').on('change', function(e) {
            e.preventDefault();

            $.ajax({
                url: "{{ route('movie.search') }}",
                type: "GET",
                data: {
                    _token: '{{ csrf_token() }}',
                    category_id: $(this).find(":selected").val(),
                    search: $('searchMovie').value,
                },
                success: function(response, textStatus, xhr) {
                    $('#movies').empty();
                    console.log(response.movies.data);
                    response.movies.data.forEach(movie => {
                        let movieDetailRoute = route('movie.detail', {
                            id: movie.id
                        });
                        var movieEle =
                            `<div class="col-md-4">
                                <div class="card m-2">
                                    <div class="card-header">
                                        <label>${movie.name}</label>
                                    </div>
                                    <div class="card-body">
                                        <a href="${movieDetailRoute}">
                                            <img src="${movie.image}" alt="movie image"
                                                style="width:170px;height:150px;">
                                        </a>
                                    </div>
                                    <div class="card-footer">
                                        <p>${movie.description.substring(0,20)}</p>
                                    </div>`;

                        movie.categories.forEach(category => {
                            movieEle += `<span class="badge rounded-pill badge-light-info" 
                                                style="font-size: 14px;color: darkgoldenrod;">${category.name}</span>`;
                        });

                        movieEle += '</div></div>';

                        $('#movies').append(movieEle);
                    });
                },
                error: function(response) {
                    alert(response);
                }
            });
        });

        $('#searchMovie').keyup(function(e) {
            e.preventDefault();

            $.ajax({
                url: "{{ route('movie.search') }}",
                type: "GET",
                data: {
                    _token: '{{ csrf_token() }}',
                    search: this.value,
                    category_id: $('#categories').find(":selected").val()
                },
                success: function(response, textStatus, xhr) {
                    $('#movies').empty();
                    console.log(response.movies.data);
                    response.movies.data.forEach(movie => {
                        let movieDetailRoute = route('movie.detail', {
                            id: movie.id
                        });
                        var movieEle =
                            `<div class="col-md-4">
                                <div class="card m-2">
                                    <div class="card-header">
                                        <label>${movie.name}</label>
                                    </div>
                                    <div class="card-body">
                                        <a href="${movieDetailRoute}">
                                            <img src="${movie.image}" alt="movie image"
                                                style="width:170px;height:150px;">
                                        </a>
                                    </div>
                                    <div class="card-footer">
                                        <p>${movie.description.substring(0,20)}</p>
                                    </div>`;

                        movie.categories.forEach(category => {
                            movieEle += `<span class="badge rounded-pill badge-light-info" 
                                    style="font-size: 14px;color: darkgoldenrod;">${category.name}</span>`;
                        });

                        movieEle += '</div></div>';

                        $('#movies').append(movieEle);
                    });
                },
                error: function(response) {
                    alert(response);
                }
            });
        });
    </script>
@endsection
