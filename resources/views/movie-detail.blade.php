@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Movies Detail</label>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('home') }}"
                                    style="display: flex;justify-content: flex-end;text-decoration: none;">Back</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <p>Name: {{ $movie->name }}</p>
                        <img src="{{ $movie->image }}" alt="movie image"
                            style="width:300px;height:300px;">
                        <p>Description: {{ $movie->description }}</p>
                        @if($movie->categories->count() > 0)
                            <h3>Categories List</h3>
                        @endif
                        @foreach ($movie->categories as $category)
                            <span class="badge rounded-pill badge-light-info" 
                                style="font-size: 14px;color: darkgoldenrod;">{{ $category->name }}</span>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
