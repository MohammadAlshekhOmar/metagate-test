<?php

namespace Database\Seeders;

use App\Models\User;
use App\Enums\GenderEnum;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'user',
            'email' => 'user@user.com',
            'email_verified_at' => now(),
            'phone' => '0993571188',
            'password' => 'p@$$word',
            'birthday' => '1990-01-01',
            'gender' =>  GenderEnum::Male->value,
            'remember_token' => Str::random(10),
        ]);

        User::factory()->count(3)->create();
    }
}
