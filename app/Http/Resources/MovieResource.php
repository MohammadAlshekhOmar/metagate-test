<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MovieResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $res['id'] = $this->id;
        $res['name'] = $this->name;
        $res['description'] = $this->description;
        $res['rate'] = $this->rate;
        $res['image'] = $this->image;
        if (!auth('api')->check()) {
            $res['user'] = UserResource::make($this->user);
        }
        $res['categories'] = CategoryResource::collection($this->categories);
        return $res;
    }
}
