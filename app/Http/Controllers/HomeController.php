<?php

namespace App\Http\Controllers;

use App\Services\CategoryService;
use App\Services\MovieService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(MovieService $movieService, CategoryService $categoryService)
    {
        $movies = $movieService->all();
        $categories = $categoryService->all();
        return view('home', [
            'movies' => $movies,
            'categories' => $categories
        ]);
    }
}
