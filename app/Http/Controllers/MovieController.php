<?php

namespace App\Http\Controllers;

use App\Services\MovieService;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    public function detail(Request $request, MovieService $movieService)
    {
        $movie = $movieService->find($request->id);
        return view('movie-detail', [
            'movie' => $movie
        ]);
    }

    public function search(Request $request, MovieService $movieService)
    {
        $movies = $movieService->search($request->search, $request->category_id);
        return response()->json([
            'movies' => $movies
        ]);
    }
}
