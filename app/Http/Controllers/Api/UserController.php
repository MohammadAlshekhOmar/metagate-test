<?php

namespace App\Http\Controllers\Api;

use App\Helpers\MyHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RegisterRequest;
use App\Http\Resources\UserResource;
use App\Services\UserService;

class UserController extends Controller
{
    public function get(UserService $userService)
    {
        $user = $userService->find(auth('api')->user()->id);
        if ($user) {
            $user = UserResource::make($user);
            return MyHelper::responseJSON(__('api.userExists'), 200, $user);
        } else {
            return MyHelper::responseJSON(__('api.unknownError'), 500);
        }
    }

    public function register(RegisterRequest $request, UserService $userService)
    {
        $user = $userService->register($request->only('name', 'email', 'phone', 'password', 'image', 'birthday', 'gender'));
        if ($user) {
            $user['user'] = UserResource::make($user['user']);
            return MyHelper::responseJSON(__('api.addSuccessfully'), 201, $user);
        } else {
            return MyHelper::responseJSON(__('api.unknownError'), 500);
        }
    }
}
