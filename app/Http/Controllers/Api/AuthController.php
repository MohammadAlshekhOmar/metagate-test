<?php

namespace App\Http\Controllers\Api;

use App\Helpers\MyHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\AuthRequest;
use App\Http\Resources\UserResource;
use App\Services\AuthService;

class AuthController extends Controller
{
    public function login(AuthRequest $request, AuthService $authService)
    {
        $user = $authService->login($request->only('username', 'password'), $reason);
        if ($user) {
            $user['user'] = UserResource::make($user['user']);
            return MyHelper::responseJSON(__('api.loginSuccessfully'), 200, $user);
        } else {
            if ($reason == 'USER_BLOCKED') {
                return MyHelper::responseJSON(__('api.userIsBlocked'), 400);
            } else if ($reason == 'INVALID_PASSWORD') {
                return MyHelper::responseJSON(__('api.passwordDontMatch'), 400);
            } else {
                return MyHelper::responseJSON(__('api.unknownError'), 500);
            }
        }
    }

    public function logout()
    {
        auth('api')->user()?->tokens()?->delete();
        return MyHelper::responseJSON(__('api.logoutSuccessfully'), 200);
    }
}
