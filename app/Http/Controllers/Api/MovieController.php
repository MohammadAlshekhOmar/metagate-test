<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\MovieService;
use App\Helpers\MyHelper;
use App\Http\Requests\Api\MovieEditRequest;
use App\Http\Requests\Api\MovieRequest;
use App\Http\Resources\MovieResource;

class MovieController extends Controller
{
    public function all(MovieService $movieService)
    {
        $movies = $movieService->all();
        if ($movies) {
            $movies = MovieResource::collection($movies);
            return MyHelper::responseJSON(__('api.movieExists'), 200, $movies);
        } else {
            return MyHelper::responseJSON(__('api.unknownError'), 500);
        }
    }

    public function add(MovieRequest $request, MovieService $movieService)
    {
        $movie = $movieService->add($request->only('name', 'description', 'rate', 'image', 'categories'), auth('api')->user()->id);
        if ($movie) {
            $movie = MovieResource::make($movie);
            return MyHelper::responseJSON(__('api.addSuccessfully'), 201, $movie);
        } else {
            return MyHelper::responseJSON(__('api.unknownError'), 500);
        }
    }

    public function edit(MovieEditRequest $request, MovieService $movieService)
    {
        $movie = $movieService->edit($request->only('id', 'name', 'description', 'rate', 'image', 'categories'));
        if ($movie) {
            $movie = MovieResource::make($movie);
            return MyHelper::responseJSON(__('api.editSuccessfully'), 200, $movie);
        } else {
            return MyHelper::responseJSON(__('api.unknownError'), 500);
        }
    }

    public function delete(MovieRequest $request, MovieService $movieService)
    {
        $movie = $movieService->delete($request->id);
        if ($movie) {
            $movie = MovieResource::make($movie);
            return MyHelper::responseJSON(__('api.deleteSuccessfully'), 200, $movie);
        } else {
            return MyHelper::responseJSON(__('api.unknownError'), 500);
        }
    }
}
