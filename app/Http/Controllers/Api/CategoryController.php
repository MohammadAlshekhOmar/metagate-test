<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\CategoryService;
use App\Helpers\MyHelper;
use App\Http\Requests\Api\CategoryRequest;
use App\Http\Resources\CategoryResource;

class CategoryController extends Controller
{
    public function all(CategoryService $categoryService)
    {
        $categories = $categoryService->all();
        if ($categories) {
            $categories = CategoryResource::collection($categories);
            return MyHelper::responseJSON(__('api.categoryExists'), 200, $categories);
        } else {
            return MyHelper::responseJSON(__('api.unknownError'), 500);
        }
    }

    public function add(CategoryRequest $request, CategoryService $categoryService)
    {
        $category = $categoryService->add($request->only('name'));
        if ($category) {
            $category = CategoryResource::make($category);
            return MyHelper::responseJSON(__('api.addSuccessfully'), 201, $category);
        } else {
            return MyHelper::responseJSON(__('api.unknownError'), 500);
        }
    }

    public function edit(CategoryRequest $request, CategoryService $categoryService)
    {
        $category = $categoryService->edit($request->only('id', 'name'));
        if ($category) {
            $category = CategoryResource::make($category);
            return MyHelper::responseJSON(__('api.editSuccessfully'), 200, $category);
        } else {
            return MyHelper::responseJSON(__('api.unknownError'), 500);
        }
    }

    public function delete(CategoryRequest $request, CategoryService $categoryService)
    {
        $category = $categoryService->delete($request->id);
        if ($category) {
            $category = CategoryResource::make($category);
            return MyHelper::responseJSON(__('api.deleteSuccessfully'), 200, $category);
        } else {
            return MyHelper::responseJSON(__('api.unknownError'), 500);
        }
    }
}
