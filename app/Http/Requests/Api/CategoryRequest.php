<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'POST') {
            return [
                'name' => 'required|string'
            ];
        } else if ($this->method() == 'PUT') {
            return [
                'id' => 'required|exists:categories,id',
                'name' => 'required|string'
            ];
        } else if ($this->method() == 'DELETE') {
            return [
                'id' => 'required|exists:categories,id'
            ];
        }
    }
}
