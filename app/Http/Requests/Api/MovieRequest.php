<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class MovieRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'POST') {
            return [
                'name' => 'required|string',
                'description' => 'required',
                'rate' => 'required|integer|between:1,5',
                'image' => 'required|image|max:4096|mimes:jpg,jpeg,png,gif|mimetypes:image/jpeg,image/png',
                'categories' => 'required|array',
                'categories.*' => 'required|exists:categories,id'
            ];
        } else if ($this->method() == 'DELETE') {
            return [
                'id' => 'required|exists:movies,id'
            ];
        }
    }
}
