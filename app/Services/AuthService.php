<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthService
{
    public function login($request, &$reason = NULL)
    {
        $type = (preg_match("/^[^@]*@[^@]*\.[^@]*$/", $request['username'])) ? 'email' : 'phone';
        if (User::onlyTrashed()->where($type, $request['username'])->exists()) {
            $reason = 'USER_BLOCKED';
            return NULL;
        } else {
            $user = User::where($type, $request['username'])->first();

            if (!Hash::check($request['password'], $user->password)) {
                $reason = 'INVALID_PASSWORD';
                return NULL;
            } else {
                return [
                    "token" => $user->createToken("Device")->plainTextToken,
                    "user" => $user,
                ];
            }
        }
    }
}
