<?php

namespace App\Services;

use App\Models\Category;

class CategoryService
{
    public function all()
    {
        return Category::paginate(8);
    }

    public function add($request)
    {
        return Category::create($request);
    }

    public function edit($request)
    {
        $category = Category::find($request['id']);
        $category->update($request);
        return $category;
    }

    public function delete($id)
    {
        $category = Category::find($id);
        $category->delete();
        return $category;
    }
}
