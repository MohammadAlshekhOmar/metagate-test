<?php

namespace App\Services;

use App\Models\Movie;

class MovieService
{
    public function all()
    {
        return Movie::with(['user', 'categories', 'media'])->latest()->paginate(9);
    }

    public function find($id)
    {
        return Movie::with(['user', 'categories', 'media'])->find($id);
    }

    public function search($search, $category_id) 
    {
        if ($category_id) {
            $movies = Movie::with(['user', 'categories', 'media'])->where('name', 'like', '%' . $search . '%')
                ->whereHas('categories', function ($q) use($category_id) {
                    $q->where('category_id', $category_id);
            })->paginate(9);
        } else {
            $movies = Movie::with(['user', 'categories', 'media'])->where('name', 'like', '%' . $search . '%')->paginate(9);
        }

        foreach($movies as $movie) {
            $movie->image = $movie->image;
        }
        return $movies;
    }

    public function add($request, $user_id)
    {
        $request['user_id'] = $user_id;
        $movie = Movie::create($request);
        $movie->addMedia($request['image'])->toMediaCollection('Movie');
        $movie->categories()->attach($request['categories']);
        $movie = Movie::with(['user', 'categories', 'media'])->find($movie->id);
        $movie->image = $movie->image;
        return $movie;
    }

    public function edit($request)
    {
        $movie = Movie::find($request['id']);
        $movie->update($request);
        $movie->categories()->sync($request['categories']);
        if (isset($request['image'])) {
            $movie->clearMediaCollection('Movie');
            $movie->addMedia($request['image'])->toMediaCollection('Movie');
        }
        $movie = Movie::with(['user', 'categories', 'media'])->find($movie->id);
        $movie->image = $movie->image;
        return $movie;
    }

    public function delete($id)
    {
        $movie = Movie::find($id);
        $movie->delete();
        return $movie;
    }
}
