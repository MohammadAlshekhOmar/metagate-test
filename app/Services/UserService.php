<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\DB;

class UserService
{
    public function all()
    {
        return User::withTrashed()->get();
    }

    public function find($id)
    {
        return User::withTrashed()->find($id);
    }

    public function register($request)
    {
        DB::beginTransaction();
        $user = User::create($request);
        $user->addMedia($request['image'])->toMediaCollection('User');
		$user = User::find($user->id);

        DB::commit();
        return [
            "token" => $user->createToken("Device")->plainTextToken,
            "user" => $user,
        ];
    }
}
