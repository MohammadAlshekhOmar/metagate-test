<?php

namespace App\Helpers;

class MyHelper
{
    public static function responseJSON($message, $code, $data = NULL)
    {
        return response()->json([
            'message' => $message,
            'data' => $data
        ], $code);
    }
}
